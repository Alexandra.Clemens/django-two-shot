from django.contrib import admin


# Register your models here.
from .models import ExpenseCategory, Account, Receipt

admin.site.register(ExpenseCategory)
admin.site.register(Account)
admin.site.register(Receipt)
